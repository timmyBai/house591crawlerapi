from fastapi import FastAPI;
from Controllers.House591 import House591Controllers;
from fastapi.middleware.cors import CORSMiddleware;

basenameRouter = '/House591Crawler';

origins = [
    '*'
];

app = FastAPI(
    version='1.0.0'
);

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
    allow_headers=['*']
);

app.include_router(House591Controllers.api, prefix=basenameRouter, tags=['house591']);
