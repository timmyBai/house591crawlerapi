from cryptography.hazmat.primitives.asymmetric import ed25519;
from cryptography.hazmat.primitives import serialization;
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey;
from pyseto import Key, Paseto, Token;
import json;
from fastapi import HTTPException, Depends, HTTPException;
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials;
import redis;
from Setting.RedisSetting import RedisSetting;

private_key_pem = b"-----BEGIN PRIVATE KEY-----\nMC4CAQAwBQYDK2VwBCIEILTL+0PfTOIQcn2VPkpxMwf6Gbt9n4UEFDjZ4RuUKjd0\n-----END PRIVATE KEY-----"
public_key_pem = b"-----BEGIN PUBLIC KEY-----\nMCowBQYDK2VwAyEAHrnbu7wEfAP9cGBOAHHwmH4Wsot1ciXBHwBBXQ4gsaI=\n-----END PUBLIC KEY-----"

exp=1800;

paseto = Paseto.new(
    include_iat=True,
    exp=exp
);


# private_key: Ed25519PrivateKey = ed25519.Ed25519PrivateKey.generate()

# private_pem: bytes = private_key.private_bytes(
#     encoding=serialization.Encoding.PEM,
#     format=serialization.PrivateFormat.PKCS8,
#     encryption_algorithm=serialization.NoEncryption()
# );

# public_key = private_key.public_key()

# public_pem = public_key.public_bytes(
#     encoding=serialization.Encoding.PEM,
#     format=serialization.PublicFormat.SubjectPublicKeyInfo
# )

# self.__private_key_pem__ = private_pem.decode();

# self.__public_key_pem__ = public_pem.decode();

private_key = Key.new(version=4, purpose="public", key=private_key_pem)
public_key = Key.new(version=4, purpose="public", key=public_key_pem)

security = HTTPBearer();

def encode(payload) -> str:

    return paseto.encode(
        private_key,
        payload,
        serializer=json
    );

def decode(data: str) -> (bytes | dict):
    decoded: Token = paseto.decode(public_key, data, deserializer=json)

    return decoded.payload;

def auth_validate(credentials: HTTPAuthorizationCredentials = Depends(security)):
    rs = redis.Redis(host=RedisSetting.host, port=RedisSetting.port, decode_responses=True);
    
    try:
        token = credentials.credentials;
        
        if token == '':
            raise HTTPException(status_code=401);

        decode = paseto.decode(public_key, token, deserializer=json);

        rs_token_context = rs.get(decode.payload['uid']);
        
        if rs_token_context == None:
            raise HTTPException(status_code=401);

        return decode.payload;
    except Exception as e:
        raise HTTPException(status_code=401);