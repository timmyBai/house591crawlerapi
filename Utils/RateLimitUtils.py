from typing import Awaitable;
from redis import Redis;
import time;

class RateLimitUtils:
    rs: Redis;

    RATE_LIMIT_TIME_WINDOW: int = 60;

    RATE_LIMIT_MAX_REQUESTS: int = 3;

    def __init__(self, rs: Redis) -> None:
        '''
            初始化連線 redis

            :param rs: redis 連線 class
            :type rs: Redis

            :return: None
            :rtype: None
        '''

        self.rs = rs;

    def is_rate_limited(self, user_id: str) -> bool:
        '''
            限制期限的限制、期限內的查詢順序、是否限制期限的決定

            :param user_id: 使用者 id
            :type user_id: str

            :return: 有無超過限制
            :rtype: bool
        '''

        current_time: int = int(time.time());

        user_requests_rate_limited_key: str = f'{user_id}_rate_limited';
        user_requests: Awaitable[list] | list = self.rs.lrange(user_requests_rate_limited_key, 0, -1);

        if len(user_requests) < self.RATE_LIMIT_MAX_REQUESTS:
            self.rs.lpush(user_requests_rate_limited_key, str(current_time));
            self.rs.expire(user_requests_rate_limited_key, self.RATE_LIMIT_TIME_WINDOW);

            return False;
        else:
            first_request_time: int = int(user_requests[0]);

            if current_time - first_request_time < self.RATE_LIMIT_TIME_WINDOW:
                return True;
            else:
                self.rs.rpush(user_requests_rate_limited_key, current_time);
                self.rs.ltrim(user_requests_rate_limited_key, -self.RATE_LIMIT_MAX_REQUESTS, -1);
                self.rs.expire(user_requests_rate_limited_key, self.RATE_LIMIT_TIME_WINDOW);
                
                return False;
