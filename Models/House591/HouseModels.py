from pydantic import BaseModel, Field;
# from typing import Optional;

class RequestHouse591AuthToken(BaseModel):
    csrfToken: str;

class RequestHouse591Detail(BaseModel):
    csrfToken: str;
    houseId: int;

class RequestRefreshToken(BaseModel):
    refreshToken: str;