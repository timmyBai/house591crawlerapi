from typing import Any;

class ResultModels:
    isSuccess: bool;
    message: str;
    data: object;

    def __init__(self) -> None:
        self.isSuccess = True;
        self.message = '';
        self.data = None;

    def to_dict(self) -> dict[str, Any]:
        return {
            'isSuccess': self.isSuccess,
            'message': self.message,
            'data': self.data
        };