from typing import Annotated, LiteralString;
from fastapi import APIRouter, status, Depends, Request, Body;
from fastapi.responses import JSONResponse;
import logging;
import requests;
from requests import Response;
from bs4 import BeautifulSoup;
from bs4.element import Tag;
import re;
from Utils.PasetoUtils import encode, decode, auth_validate;
import uuid;
from datetime import datetime, timezone, timedelta;
import redis;
import json;
from Utils.PasetoUtils import exp;
from Models.ResultModels import ResultModels;
from Models.House591.HouseModels import RequestHouse591AuthToken, RequestHouse591Detail, RequestRefreshToken;
from VM.House591.House591VM import House591CsrfTokenVM, House591RoomTypeDetailedVM, AuthTokenVM, RefreshTokenVM, RedisAuthTokenVM;
from Utils.RateLimitUtils import RateLimitUtils;
import time;
from Setting.RedisSetting import RedisSetting;

api = APIRouter();

'''
    訪問登入
'''
@api.post('/api/auth/login')
def AuthLogin(request: Request) -> JSONResponse:
    result: ResultModels = ResultModels();
    rs = redis.Redis(host=RedisSetting.host, port=RedisSetting.port, decode_responses=True);

    try:
        user_id = request.cookies.get('user_id') or str(uuid.uuid4());

        # 檢查是否超過頻率限制
        if RateLimitUtils(rs).is_rate_limited(user_id):
            result.isSuccess = False;
            result.message = 'TooManyRequests';
            logging.error('TooManyRequests');
            return JSONResponse(result.to_dict(), status.HTTP_429_TOO_MANY_REQUESTS);

        rs_user_id = rs.get(f'{user_id}');
        
        authToken: AuthTokenVM = AuthTokenVM();

        if rs_user_id != None:
            rs_user_id_json = json.loads(rs_user_id);
            authToken.accessToken = rs_user_id_json['accessToken'];
            authToken.refreshToken = rs_user_id_json['refreshToken'];
            authToken.tokenType = rs_user_id_json['tokenType'];
            authToken.expiresIn = rs_user_id_json['expiresIn'];

            result.isSuccess = True;
            result.data = authToken.to_dict();
            return JSONResponse(result.to_dict(), status_code=status.HTTP_200_OK);
        else:
            authToken.accessToken = str(encode({
                'uid': user_id
            }), encoding='utf-8');
            authToken.refreshToken = str(uuid.uuid4());
            authToken.tokenType = 'Bearer';
            authToken.expiresIn = int((datetime.now(timezone.utc) + timedelta(seconds=exp)).timestamp()) * 1000;
            redisAuthToken: RedisAuthTokenVM = RedisAuthTokenVM();
            redisAuthToken.accessToken = authToken.accessToken;
            redisAuthToken.refreshToken = authToken.refreshToken;
            redisAuthToken.tokenType = authToken.tokenType;
            redisAuthToken.expiresIn = authToken.expiresIn;

            rs.set(user_id, json.dumps(redisAuthToken.to_dict()), ex=exp);

            result.isSuccess = True;
            result.data = authToken.to_dict();
            response = JSONResponse(result.to_dict(), status.HTTP_200_OK);
            response.set_cookie(
                key='user_id',
                value=user_id,
                max_age=exp,
                samesite='strict'
            );

            return response;
    except Exception as ex:
        result.isSuccess = False;
        result.message = 'InternalServerError';
        logging.error(f'InternalServerError: {ex}');
        return JSONResponse(result.to_dict(), status.HTTP_500_INTERNAL_SERVER_ERROR);

'''
    取得訪問者簡介
'''
@api.post('/api/introduction')
def Introduction(request: Request, dependencies=Depends(auth_validate)) -> JSONResponse:
    result: ResultModels = ResultModels();
    
    try:
        token = request.headers.get('Authorization').split(' ')[1];

        result.isSuccess = True;
        result.data = decode(token);
        logging.info('VisitorDecodeSuccess');
        return JSONResponse(result.to_dict(), status.HTTP_200_OK);
    except Exception as ex:
        result.isSuccess = False;
        result.message = 'InternalServerError';
        logging.error(f'InternalServerError: {ex}');
        return JSONResponse(result.to_dict(), status.INTERNAL_SERVER_ERROR);

'''
    取得 591 租屋令牌
'''
@api.post('/api/auth/house591/token')
def AuthHouse591Token(dependencies=Depends(auth_validate)) -> JSONResponse:
    result: ResultModels = ResultModels();

    try:
        # 取得 591 租屋 token 網頁
        resTokenDocument: Response = requests.get('https://rent.591.com.tw/?region=1&multiPrice=5000_10000', headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36',
            'Cookie': 'webp=1; PHPSESSID=mjq618830j85da0mtsdls8qsbn; T591_TOKEN=mjq618830j85da0mtsdls8qsbn; newUI=1; new_rent_list_kind_test=0; _gid=GA1.3.924375422.1712708475; _ga=GA1.4.581020951.1712708475; _gid=GA1.4.924375422.1712708475; tw591__privacy_agree=0; __lt__cid=ba7b145d-d2f3-4e3d-abae-c20aaf38fff8; _fbp=fb.2.1712708476189.654498388; is_new_index=1; is_new_index_redirect=1; index_keyword_search_analysis=%7B%22role%22%3A%220%22%2C%22type%22%3A1%2C%22keyword%22%3A%22%22%2C%22selectKeyword%22%3A%22%22%2C%22menu%22%3A%22%22%2C%22hasHistory%22%3A0%2C%22hasPrompt%22%3A0%2C%22history%22%3A0%7D; urlJumpIp=1; XSRF-TOKEN=eyJpdiI6IkNaTkd1MFExU1lpdzV4L3NDSWNualE9PSIsInZhbHVlIjoiSllxaVZLL1NQVFkwYjdzMDZ0WlM2Z2pqcWtITnAvc2c1V0l3ZGJFbWFUdWNTRzh4SEpBU3h4MkVyV2tocHcyUVdHTmF5bit0S1ZUYndidFhTc05hRmNRMjlVenFpTTZvNWVvMGYzbWw2WHJ1aUZpVDc0aTR5eDk4VWpCbE5JbVEiLCJtYWMiOiI5ZjU4MmU1Y2NmMzkwMWQxYjdmOGYwMzQ2OThiNjcxYTQ0OTFjNTQ2NDA5NTUwNzRmZGNkNmQ2ODM2ZWRlNDM0IiwidGFnIjoiIn0%3D; 591_new_session=eyJpdiI6InFQcmpVcU9wNkhCc0twQXpOTEF3Ync9PSIsInZhbHVlIjoiUTIyKzlYdjYyd1ZSUFZsVjY0dDZOSXhoNXRPalFVWHJXTzBSQWhZQWZ2Rmx5a1pvdjFSYkxxTTZvTXNtN3E3cXZocFkralVWaUw0SS9YQ0svZ2RMMzRHdFhjaXpDelk4Ykl3UnlxS1lsZGhEa1UramFNQ1Uzd0IxbFZGSWlBMTQiLCJtYWMiOiJhZmM2YzYwNjdhYmYwMTdiNGI5MTUyODE1ZmQ3MWE4MDBmZDJhMzJjNmJiZWZlNmQ4MDNlNmRiOTZlZTcxOTUyIiwidGFnIjoiIn0%3D; last_search_type=12; _gat=1; _dc_gtm_UA-97423186-1=1; _gat_UA-97423186-1=1; _ga_H07366Z19P=GS1.3.1712732677.3.1.1712735023.60.0.0; _ga=GA1.3.581020951.1712708475; _ga_HDSPSZ773Q=GS1.1.1712735023.3.0.1712735026.0.0.0; timeDifference=40'
        });

        # 解析 591 租屋 token 網頁資訊
        soup: BeautifulSoup = BeautifulSoup(resTokenDocument.text);
        
        # 取得 token
        csrfToken: str = soup.select('meta[name=csrf-token]')[0].get('content');

        if (csrfToken == ''):
            result.isSuccess = True;
            result.message = 'NotGetToken';
            logging.error('NotGetToken');
            return JSONResponse(result.to_dict(), status.HTTP_403_FORBIDDEN);
        
        house591CsrfToken: House591CsrfTokenVM = House591CsrfTokenVM();
        house591CsrfToken.csrfToken = csrfToken;
        result.isSuccess = True;
        result.data = house591CsrfToken.to_dict();
        logging.info('GetHouseTokenSuccess');
        return JSONResponse(result.to_dict(), status.HTTP_200_OK);
    except Exception as ex:
        result.isSuccess = False;
        result.message = 'InternalServerError';
        logging.error(f'InternalServerError: {ex}');
        return JSONResponse(result.to_dict(), status.HTTP_500_INTERNAL_SERVER_ERROR);

'''
    取得租屋相關資料
'''
@api.post('/api/house591/room_type/list')
def GetHouse591RoomTypeList(body: RequestHouse591AuthToken, dependencies=Depends(auth_validate)) -> JSONResponse:
    result: ResultModels = ResultModels();

    try:
        # 取得基本篩選過後租屋清單
        houseList: Response = requests.get('https://rent.591.com.tw/home/search/rsList?is_format_data=1&is_new_list=1&type=1&region=1&multiPrice=5000_10000&recom_community=1', headers={
            'X-Csrf-Token': body.csrfToken,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36',
            'Cookie': 'webp=1; PHPSESSID=mjq618830j85da0mtsdls8qsbn; T591_TOKEN=mjq618830j85da0mtsdls8qsbn; newUI=1; new_rent_list_kind_test=0; _gid=GA1.3.924375422.1712708475; _ga=GA1.4.581020951.1712708475; _gid=GA1.4.924375422.1712708475; tw591__privacy_agree=0; __lt__cid=ba7b145d-d2f3-4e3d-abae-c20aaf38fff8; _fbp=fb.2.1712708476189.654498388; is_new_index=1; is_new_index_redirect=1; index_keyword_search_analysis=%7B%22role%22%3A%220%22%2C%22type%22%3A1%2C%22keyword%22%3A%22%22%2C%22selectKeyword%22%3A%22%22%2C%22menu%22%3A%22%22%2C%22hasHistory%22%3A0%2C%22hasPrompt%22%3A0%2C%22history%22%3A0%7D; urlJumpIp=1; XSRF-TOKEN=eyJpdiI6IkNaTkd1MFExU1lpdzV4L3NDSWNualE9PSIsInZhbHVlIjoiSllxaVZLL1NQVFkwYjdzMDZ0WlM2Z2pqcWtITnAvc2c1V0l3ZGJFbWFUdWNTRzh4SEpBU3h4MkVyV2tocHcyUVdHTmF5bit0S1ZUYndidFhTc05hRmNRMjlVenFpTTZvNWVvMGYzbWw2WHJ1aUZpVDc0aTR5eDk4VWpCbE5JbVEiLCJtYWMiOiI5ZjU4MmU1Y2NmMzkwMWQxYjdmOGYwMzQ2OThiNjcxYTQ0OTFjNTQ2NDA5NTUwNzRmZGNkNmQ2ODM2ZWRlNDM0IiwidGFnIjoiIn0%3D; last_search_type=12; _gat=1; _dc_gtm_UA-97423186-1=1; _gat_UA-97423186-1=1; timeDifference=40; 591_new_session=eyJpdiI6Ii92TEtPY0xLOTBuUXFRejlNOUY4VFE9PSIsInZhbHVlIjoiWERvYVpkWTh0eWNGL1F4emxKMzkwaTFxNU5VKzlMMUo5R2gwMUVzU1NzMzNRMkJPaHNDZ0RaSkhGaTE0QmpjTEN5UGhxYW1OZytYaEZaOXlCSTkrTGZlR3N2c0YvZDZ3RFpINDVPK01XVzZyTzJINk96NVBRMklOQ0FZcFc2NEQiLCJtYWMiOiJiMDBmYjQ1M2U5YzdlNGMxZGNhNDFhNzQ2ZWI5YWVlYmE1MTVjMjFkZGI4Njc0ZjFlYzU3ZDMyN2YwZmJmY2ZiIiwidGFnIjoiIn0%3D; __lt__sid=7641f551-bb1af467; _ga=GA1.1.581020951.1712708475; _ga_H07366Z19P=GS1.3.1712732677.3.1.1712735032.51.0.0; _ga_HDSPSZ773Q=GS1.1.1712735023.3.1.1712735033.0.0.0'
        });

        if houseList.text == '':
            result.isSuccess = True;
            result.message = 'NotHouseInformation';
            logging.error('NotHouseInformation');
            return JSONResponse(result.to_dict(), status.HTTP_403_FORBIDDEN);

        result.isSuccess = True;
        result.data = json.loads(houseList.text);
        logging.info('GetHouseList');
        return JSONResponse(result.to_dict(), status.HTTP_200_OK);
    except Exception as ex:
        result.isSuccess = False;
        result.message = 'InternalServerError';
        logging.error(f'InternalServerError: {ex}');
        return JSONResponse(result.to_dict(), status.INTERNAL_SERVER_ERROR);

'''
    取得租屋房型詳細資訊
'''
@api.post('/api/house/room/type/detail')
def GetHouseRoomTypeDetail(body: Annotated[RequestHouse591Detail, Body()], dependencies=Depends(auth_validate)) -> JSONResponse:
    result: ResultModels = ResultModels();

    try:
        req: str = f'https://rent.591.com.tw/{body.houseId}';

        res: Response = requests.get(req,
            headers={
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36'
            }
        );
        res.encoding = 'utf-8';

        if (res.status_code == 404):
            result.isSuccess = False;
            result.message = 'RoomTypeInformationNotFound';
            logging.error('RoomTypeInformationNotFound');
            return result;
    
        soup: BeautifulSoup = BeautifulSoup(res.text, 'html.parser');

        # 房屋標題
        title = soup.select('.house-title h1')[0].text;
        
        # 租金/租金單位
        rectInfo: str = soup.select('.house-price span.price')[0].text;
        rent: LiteralString = ''.join(re.findall(r'[0-9]', rectInfo));
        rentalUnit: LiteralString = ''.join(re.findall(r'[^0-9^,]', rectInfo));
    
        # 押金
        deposit: str = soup.select('.house-price')[0].text.split(' ')[1];

        # 房型圖片
        roomTypePicture: list[str] = [];
        for index in range(0, len(soup.select('.main .album .common-img'))):
            roomTypePicture.append(soup.select('.common-img')[index]['data-src']);
        
        # 租房類型
        roomType: str = soup.select('.house-pattern span:nth-child(1)')[0].text;
        
        # 坪數
        area: str = soup.select('.house-pattern span:nth-child(3)')[0].text;
        
        # 樓層
        floor: str = soup.select('.house-pattern span:nth-child(5)')[0].text;

        # 租房型態
        roomShape: str = soup.select('.house-pattern span:nth-child(7)')[0].text;
        
        # 屋主貴姓
        ownerSurname: str = soup.select('.main .main-wrapper .aside .contact-card .contact .contact-info .base-info .name')[0].text.replace('屋主: ', '');

        # 租屋電話
        phone: str = soup.select('.main .main-wrapper .aside .contact-card .phone .contact-action-lg .t5-button.t5-button--large.t5-button--info span:nth-child(2) span')[0].text;
        
        # 地址
        city: str = soup.select('.main .wrapper .crumbs .t5-link')[0].text;
        address: str = soup.select('.main .main-wrapper .main-content .block.surround .address p span:nth-child(2)')[0].text;
        currentAddress: str = city + address;

        # 租住說明
        rentalInstructions: str = soup.select('.main .main-wrapper .main-content .block.service div:nth-child(2) div span')[0].text;

        # 房屋守則
        houseRules: str = soup.select('.main .main-wrapper .main-content .block.service div:nth-child(3) div span')[0].text;

        # 租金包含費用
        rentIncludesFees: list = [];
        rentIncludesFeesInfo: Tag = soup.select('.main .main-wrapper .main-content .block.house-detail div .house-detail-content-left .content.left div:nth-child(1) span.value')[0];

        for element in rentIncludesFeesInfo:
            if element.name != 'i':
                rentIncludesFees.append(element);
        
        # 法定用途
        legalPurposes: str = soup.select('.main .main-wrapper .main-content .block.house-detail div .house-detail-content-right .content.right div:nth-child(1) span.value')[0].text.replace(' ', '');

        # 產權登記
        propertyRightsRegistration: str = soup.select('.main .main-wrapper .main-content .block.house-detail div .house-detail-content-right .content.right div:nth-child(2) span.value')[0].text.replace(' ', '');

        # 提供設備
        provideEquipment: list[dict[str, str]] = [
            {
                'type': '冰箱',
                'offer': 'N'
            },
            {
                'type': '洗衣機',
                'offer': 'N'
            },
            {
                'type': '電視',
                'offer': 'N'
            },
            {
                'type': '冷氣',
                'offer': 'N'
            },
            {
                'type': '熱水器',
                'offer': 'N'
            },
            {
                'type': '床',
                'offer': 'N'
            },
            {
                'type': '衣櫃',
                'offer': 'N'
            },
            {
                'type': '第四台',
                'offer': 'N'
            },
            {
                'type': '網路',
                'offer': 'N'
            },
            {
                'type': '天然瓦斯',
                'offer': 'N'
            },
            {
                'type': '沙發',
                'offer': 'N'
            },
            {
                'type': '桌椅',
                'offer': 'N'
            },
            {
                'type': '3 陽台',
                'offer': 'N'
            },
            {
                'type': '電梯',
                'offer': 'N'
            },
            {
                'type': '平面車位',
                'offer': 'N'
            }
        ];

        provideEquipmentElements = soup.select('.main .main-wrapper .main-content .block.service .facility.service-facility dl');

        for item in range(0, len(provideEquipmentElements)):
            provideEquipmentElement = provideEquipmentElements[item];
            
            if len(provideEquipmentElement.get('class')) == 1:
                if provideEquipmentElement.text == provideEquipment[item]['type']:
                    provideEquipment[item]['offer'] = 'N';
            else:
                provideEquipment[item]['offer'] = 'Y';

        house591RoomTypeDetailed: House591RoomTypeDetailedVM = House591RoomTypeDetailedVM();
        house591RoomTypeDetailed.title = title;
        house591RoomTypeDetailed.rent = rent;
        house591RoomTypeDetailed.rentalUnit = rentalUnit;
        house591RoomTypeDetailed.deposit = deposit;
        house591RoomTypeDetailed.roomTypePicture = roomTypePicture;
        house591RoomTypeDetailed.area = area;
        house591RoomTypeDetailed.floor = floor;
        house591RoomTypeDetailed.roomType = roomType;
        house591RoomTypeDetailed.roomShape = roomShape;
        house591RoomTypeDetailed.ownerSurname = ownerSurname;
        house591RoomTypeDetailed.phone = phone;
        house591RoomTypeDetailed.currentAddress = currentAddress;
        house591RoomTypeDetailed.rentalInstructions = rentalInstructions;
        house591RoomTypeDetailed.houseRules = houseRules;
        house591RoomTypeDetailed.rentIncludesFees = '、'.join(rentIncludesFees);
        house591RoomTypeDetailed.legalPurposes = legalPurposes;
        house591RoomTypeDetailed.propertyRightsRegistration = propertyRightsRegistration;

        result.isSuccess = True;
        result.data = house591RoomTypeDetailed.to_dict();
        logging.info('GetHouse591RoomTypeDetailedSuccess');
        return JSONResponse(result.to_dict(), status.HTTP_200_OK);
    except Exception as ex:
        result.isSuccess = False;
        result.message = 'InternalServerError';
        logging.error(f'InternalServerError: {ex}');
        return JSONResponse(result.to_dict(), status.INTERNAL_SERVER_ERROR);

'''
    重洗令牌
'''
@api.post('/api/auth/refresh/token')
def RefreshToken(request: Request, body: RequestRefreshToken, dependencies=Depends(auth_validate)) -> JSONResponse:
    result: ResultModels = ResultModels();
    rs = redis.Redis(host=RedisSetting.host, port=RedisSetting.port, decode_responses=True);

    try:
        token: str = request.headers.get('Authorization').split(' ')[1];
        user_id: str = request.cookies.get('user_id') or str(uuid.uuid4());

        data: bytes | dict = decode(token);

        old_uid = data['uid'];

        rs_old_uid = rs.get(data['uid']);

        rs_old_uid_json = json.loads(rs_old_uid);

        current_time = int(time.time()) * 1000;
        expiresIn = rs_old_uid_json['expiresIn'];
        tokenRemainingTime = int((expiresIn - current_time) / 1000 / 60);

        refreshToken: RefreshTokenVM = RefreshTokenVM();

        if rs_old_uid == None:
            result.isSuccess = True;
            result.message = 'UnAuthorized';
            logging.info('UnAuthorized');
            return JSONResponse(result.to_dict(), status.HTTP_401_UNAUTHORIZED);

        if RateLimitUtils(rs).is_rate_limited(user_id):
            result.isSuccess = True;
            result.message = 'TooManyRequests';
            logging.error('TooManyRequests');
            return JSONResponse(result.to_dict(), status_code=status.HTTP_429_TOO_MANY_REQUESTS);

        if tokenRemainingTime < 5 and body.refreshToken == rs_old_uid_json['refreshToken']:
            rs.delete(old_uid);
            rs.delete(f'{old_uid}_rate_limited');

            user_id = str(uuid.uuid4());
            
            refreshToken.accessToken = str(encode({
                'uid': user_id
            }), encoding='utf-8');
            refreshToken.refreshToken = str(uuid.uuid4());
            refreshToken.tokenType = 'Bearer';
            refreshToken.expiresIn = int((datetime.now(timezone.utc) + timedelta(minutes=30)).timestamp()) * 1000;

            redisAuthToken: RedisAuthTokenVM = RedisAuthTokenVM();
            redisAuthToken.accessToken = refreshToken.accessToken;
            redisAuthToken.refreshToken = refreshToken.refreshToken;
            redisAuthToken.tokenType = refreshToken.tokenType;
            redisAuthToken.expiresIn = refreshToken.expiresIn;

            rs.set(user_id, json.dumps(redisAuthToken.to_dict()), ex=exp);
        
            rs.lpush(f'{user_id}_rate_limited', str(current_time));
            rs.expire(f'{user_id}_rate_limited', RateLimitUtils.RATE_LIMIT_TIME_WINDOW);

            result.isSuccess = True;
            result.data = refreshToken.to_dict();
            response = JSONResponse(result.to_dict(), status.HTTP_200_OK);
            response.set_cookie(
                key='user_id',
                value=user_id,
                max_age=exp,
                samesite='strict'
            );
            logging.info('GetRefreshTokenSuccess');

            return response;
        else:
            refreshToken.accessToken = rs_old_uid_json['accessToken'];
            refreshToken.refreshToken = rs_old_uid_json['refreshToken'];
            refreshToken.tokenType = rs_old_uid_json['tokenType'];
            refreshToken.expiresIn = rs_old_uid_json['expiresIn'];

            result.isSuccess = False;
            result.data = refreshToken.to_dict();
            logging.info('OriginToken');
            return JSONResponse(result.to_dict(), status.HTTP_200_OK);
    except Exception as ex:
        result.isSuccess = False;
        result.message = 'InternalServerError';
        logging.error(f'InternalServerError: {ex}');
        return JSONResponse(result.to_dict(), status.INTERNAL_SERVER_ERROR);