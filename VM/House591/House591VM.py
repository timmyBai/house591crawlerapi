from typing import Any

class AuthTokenVM:
    accessToken: str;
    refreshToken: str;
    tokenType: str;
    expiresIn: str;

    def __init__(self) -> None:
        self.accessToken = '';
        self.refreshToken = '';
        self.tokenType = 'Bearer';
        self.expiresIn = '';


    def to_dict(self) -> dict[str, Any]:
        return {
            'accessToken': self.accessToken,
            'refreshToken': self.refreshToken,
            'tokenType': 'Bearer',
            'expiresIn': self.expiresIn
        }
    
class RedisAuthTokenVM:
    accessToken: str;
    refreshToken: str;
    tokenType: str;
    expiresIn: int;

    def __init__(self) -> None:
        self.accessToken = '';
        self.refreshToken = '';
        self.tokenType = 'Bearer';
        self.expiresIn = '';
        
    def to_dict(self) -> dict[str, Any]:
        return {
            'accessToken': self.accessToken,
            'refreshToken': self.refreshToken,
            'tokenType': 'Bearer',
            'expiresIn': self.expiresIn
        }

class House591CsrfTokenVM:
    csrfToken: str;

    def __init__(self) -> None:
        self.csrfToken = '';

    def to_dict(self) -> dict[str, Any]:
        return {
            'csrfToken': self.csrfToken
        }

class House591RoomTypeDetailedVM:
    # 租屋標題
    title: str;

    # 租金
    rent: str;

    # 租金單位
    rentalUnit: str;

    # 押金
    deposit: str;

    # 房型照片
    roomTypePicture: list[str];

    # 坪數
    area: str;

    # 樓層
    floor: str;

    # 租房類型
    roomType: str;

    # 租房型態
    roomShape: str;

    # 屋主貴姓
    ownerSurname: str;

    # 租屋電話
    phone: str;

    # 地址
    currentAddress: str;

    # 租住說明
    rentalInstructions: str;

    # 房屋守則
    houseRules: str;

    # 租金包含費用
    rentIncludesFees: str;

    # 法定用途
    legalPurposes: str;

    # 產權登記
    propertyRightsRegistration: str;

    # 
    
    def __init__(self) -> None:
        self.title = '';
        self.rent = '';
        self.rentalUnit = '';
        self.roomTypePicture = [];
        self.area = '';
        self.roomType = '';
        self.roomShape = '';
        self.floor = '';
        self.ownerSurname = '';
        self.phone = '';
        self.currentAddress = '';
        self.rentalInstructions = '';
        self.houseRules = '';
        self.rentIncludesFees = '';
        self.legalPurposes = '';
        self.propertyRightsRegistration = '';


    def to_dict(self) -> dict[str, Any]:
        return {
            'title': self.title,
            'rent': self.rent,
            'rentalUnit': self.rentalUnit,
            'deposit': self.deposit,
            'roomTypePicture': self.roomTypePicture,
            'area': self.area,
            'floor': self.floor,
            'roomType': self.roomType,
            'roomShape': self.roomShape,
            'ownerSurname': self.ownerSurname,
            'phone': self.phone,
            'currentAddress': self.currentAddress,
            'rentalInstructions': self.rentalInstructions,
            'houseRules': self.houseRules,
            'rentIncludesFees': self.rentIncludesFees,
            'legalPurposes': self.legalPurposes,
            'propertyRightsRegistration': self.propertyRightsRegistration
        }

class RefreshTokenVM:
    accessToken: str;
    refreshToken: str;
    tokenType: str;
    expiresIn: str;

    def __init__(self) -> None:
        self.accessToken = '';
        self.refreshToken = '';
        self.tokenType = 'Bearer';
        self.expiresIn = '';

    def to_dict(self) -> dict[str, Any]:
        return {
            'accessToken': self.accessToken,
            'refreshToken': self.refreshToken,
            'tokenType': 'Bearer',
            'expiresIn': self.expiresIn
        }